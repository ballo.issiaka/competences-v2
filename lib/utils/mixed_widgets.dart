import 'package:competences/utils/palettes.dart';
import 'package:flutter/material.dart';

lineEnd () {
  return Center(
    child: Column(
      children: <Widget>[
        SizedBox(height: 10,),
        Icon(Icons.adjust, color: Colors.grey,),
        SizedBox(height: 10,)
      ],
    ),
  );
}
appNameColored () {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Text(
        "Compé",
        style: TextStyle(
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Text(
        "tences",
        style: TextStyle(
            color: Palette.primary,
            fontSize: 25.0,
            fontWeight: FontWeight.bold),
      ),
    ],
  );
}
roundedImage(getImage) {
  final userImage = Container(
    width: 150,
    height: 150.0,
    decoration: BoxDecoration(
      image: DecorationImage(
        image: AssetImage(getImage),
        fit: BoxFit.cover,
      ),
      borderRadius: BorderRadius.circular(100),
      border: Border.all(
        color: Palette.primary,
        width: 3,
      ),
    ),
  );
  return userImage;
}
navbar(context, title) {
  return AppBar(
      title: Center(
        child: Text(title),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.edit, color: Palette.secondary,),
          onPressed: ()=>{

          },
        )
      ]
  );
}