import 'package:flutter/material.dart';

class TitleWidget extends StatefulWidget {
  String title, theme = 'light';
  Color color;
  double size;
  TitleWidget({this.title, this.size, this.theme});

  @override
  _State createState() => _State();
}
class _State extends State<TitleWidget>{
  @override
  Widget build(BuildContext context) {

    widget.color = (widget.theme == 'dark' ? Colors.black : Colors.white);
    widget.title = (widget.title == null || widget.title == '' ? 'nodata' : widget.title);
    widget.size = (widget.size == null || widget.size == 0 ? 28.0 : widget.size);

    return Container(
      child: Text(widget.title, style: TextStyle(fontSize: widget.size, fontWeight: FontWeight.bold, color: widget.color),),
    );
  }
}
