import 'package:flutter/material.dart';

class Palette {
  static Color primary = Color(0xfffed102);
  static Color secondary = Color(0xff6d2d11);
  static Color dark = Color(0xff1B1B1B);
  static Color white = Color(0xffffffff);

  static Color danger = Color(0xffC04442);
  static Color warning = Color(0xff8A6D3B);
  static Color info = Color(0xff3186C2);
  static Color success = Color(0xff5B765B);


}