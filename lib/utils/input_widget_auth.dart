
import 'package:competences/utils/palettes.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class InputWidgetAuth extends StatefulWidget {
  var txController, type, line, prefixIcon, label, hint, isEmptyText, isPassField, readOnly;
  InputWidgetAuth({this.txController,this.type, this.line, this.prefixIcon, this.label, this.hint, this.isEmptyText, this.isPassField, this.readOnly});
  @override
  _InputWidgetAuthState createState() => _InputWidgetAuthState();
}

class _InputWidgetAuthState extends State<InputWidgetAuth> {

  @override
  Widget build(BuildContext context) {
    bool passfield = false;
    bool readOnly = false;
    if (widget.isPassField == true) {
      passfield = true;
    }
    if(widget.readOnly == true) {
      readOnly = true;
    }
    return TextFormField(
      style: TextStyle(fontSize: 12),
      readOnly: readOnly,
      obscureText: passfield,
      controller: widget.txController,
      keyboardType: widget.type,
      maxLines: widget.line,
      decoration: new InputDecoration(
        errorStyle: TextStyle(color: Colors.white, fontSize: 14),
        filled: true,
        fillColor: Colors.white70,
        labelText: widget.label,
        hintText: widget.hint,
        prefixIcon: Icon(widget.prefixIcon, color: Palette.secondary),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Palette.secondary),
          borderRadius: BorderRadius.circular(30),
        ),
        border: OutlineInputBorder(
            borderSide: BorderSide(color: Palette.secondary),
            borderRadius: BorderRadius.circular(30)
        ),
      ),
      validator: (value) {
        if (value.isEmpty && widget.isEmptyText != false) {
          return widget.isEmptyText;
        }
        return null;
      },
    );
  }
}