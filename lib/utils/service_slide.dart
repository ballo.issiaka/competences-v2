import 'package:flutter/material.dart';
import 'package:competences/utils/model.dart';
import 'package:competences/utils/T2Colors.dart';
import 'package:competences/utils/T2Constant.dart';
import 'package:competences/utils/T2DataGenerator.dart';
import 'package:competences/utils/T2SliderWidget.dart';
import 'package:competences/utils/palettes.dart';
import 'package:competences/utils/T2Widgets.dart';
class ServiceSlide extends StatefulWidget {
  @override
  _ServiceSlideState createState() => _ServiceSlideState();
}

class _ServiceSlideState extends State<ServiceSlide> {
  var currentIndexPage = 0;
  List<T2Slider> mSliderList;
   @override
  void initState() {
    super.initState();
    mSliderList = getSliders();

  }
  @override
  Widget build(BuildContext context) {
    
    var width = MediaQuery.of(context).size.width;
    width = width - 50;
    final Size cardSize = Size(width, width / 1.8);
    return Column(
      children: <Widget>[
        T2CarouselSlider(
          viewportFraction: 0.4,
          height: cardSize.height,
          enlargeCenterPage: true,
          scrollDirection: Axis.horizontal,
          items: mSliderList.map((slider) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                  width: MediaQuery.of(context).size.width,
                  height: cardSize.height,
                  margin: EdgeInsets.symmetric(horizontal: 8.0),
                  child: Stack(
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: new BorderRadius.circular(12.0),
                        child: Image.asset(
                          'images/commons/logo.png',
                          fit: BoxFit.fill,
                          width: MediaQuery.of(context).size.width,
                          height: cardSize.height,
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          padding: EdgeInsets.all(8.0),
                          constraints: BoxConstraints(minWidth: double.infinity),
                          decoration: BoxDecoration(
                            color: Palette.dark.withOpacity(0.8),
                          ),
                          child: Text('title service', style: TextStyle(color: Palette.primary),),
                        ),
                        )
                    ],
                  ),
                );
              },
            );
          }).toList(),
          onPageChanged: (index){
            setState(() {
              currentIndexPage=index;
            });
          },
        ),
      ],
      
    );
  }
}