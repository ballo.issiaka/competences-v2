import 'package:competences/routes/routes.dart';
import 'package:competences/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:competences/utils/palettes.dart';


class MyApp extends StatefulWidget {
  static const String routeName = '/myApp';

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appName,
      theme: ThemeData(
        // Define the default brightness and colors.
        //brightness: Brightness.dark,
        fontFamily: "Lubalin",
        primaryColor: Palette.primary,
      ),
      initialRoute: "/",
      routes: Router.routeArray,
    );
  }
}
