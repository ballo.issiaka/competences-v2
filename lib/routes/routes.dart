import 'package:competences/views/home/home_page.dart';
import 'package:competences/views/profile/profile_screen.dart';
import 'package:competences/views/reg/client/reg_client_screen.dart';
import 'package:competences/views/reg/pro/reg_pro_screen.dart';
import 'package:competences/views/reg/reg_type_screen.dart';
import 'package:competences/views/user/auth_screen.dart';
import 'package:competences/views/user/signup_signin.dart';
import 'package:flutter/material.dart';

class Router {

  static var routeArray = <String, WidgetBuilder>{
    
    "/": (BuildContext context) => new SignPage(),
    "/auth": (BuildContext context) => new AuthScreen(),
    "/home": (BuildContext context) => new HomePage(),
    "/reg/type": (BuildContext context) => new RegTypeScreen(),
    "/reg/client": (BuildContext context) => new RegClientScreen(),
    "/reg/pro": (BuildContext context) => new RegProScreen(),
    "/profile": (BuildContext context) => new ProfileScreen(),
  };
}