import 'package:competences/utils/palettes.dart';
import 'package:competences/utils/title_widget.dart';
import 'package:flutter/material.dart';
import 'package:competences/utils/constants.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _State createState() => _State();
}
class _State extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  child: Container(
                    color: Palette.white,
                    child: Column(
                      children: <Widget>[
                        Stack(fit: StackFit.loose, children: <Widget>[
                          new Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Container(
                                  width: MediaQuery.of(context).size.width/2,
                                  height: MediaQuery.of(context).size.width/2,
                                  decoration: new BoxDecoration(
                                    border: Border.all(color: Palette.primary, width: 3),
                                    shape: BoxShape.circle,
                                    image: new DecorationImage(
                                      image: AssetImage('images/commons/logo.png'),
                                      fit: BoxFit.cover,
                                    ),
                                  )
                              ),
                            ],
                          ),
                        ]),
                        TitleWidget(title: 'John Doe', theme: 'dark')
                      ],
                    ),
                  )
                ),
                SizedBox(height: 10,),
                Container(
                  width: double.infinity,
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(MediaQuery.of(context).size.width/40),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Recommandations', style: TextStyle(fontSize: 16),),
                            Row(
                              children: <Widget>[
                                Icon(Icons.star),
                                Icon(Icons.star),
                                Icon(Icons.star),
                                Icon(Icons.star),
                                Icon(Icons.star_half),
                              ],
                            ),
                            Divider(),
                            Text('Nom & Prénoms', style: TextStyle(fontSize: 12),),
                            TitleWidget(title: 'John Doe', theme: 'dark', size: 16,),
                            Divider(),
                            Text('Domaine d\'activité et Spécialités', style: TextStyle(fontSize: 12),),
                            TitleWidget(title: 'Plomberie, Tuyauterie', theme: 'dark', size: 16,),
                            Divider(),
                            Text('Localisation', style: TextStyle(fontSize: 12),),
                            TitleWidget(title: 'Angré Djibi 8e Tranche', theme: 'dark', size: 16,),
                            Divider(),
                            Text('Contacts', style: TextStyle(fontSize: 12),),
                            TitleWidget(title: '09 78 77 53', theme: 'dark', size: 16,),
                            Divider(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(icon: Icon(Icons.call), onPressed: () {

              },),
              IconButton(icon: Icon(Icons.message), onPressed: () {

              },),
              IconButton(icon: Icon(Icons.group), onPressed: () {

              },),
              IconButton(icon: Icon(Icons.note), onPressed: () {

              },),
              IconButton(icon: Icon(Icons.thumb_up), onPressed: () {

              },),
            ],
          ),
              ],
            ),
          ),
    );
  }
}
