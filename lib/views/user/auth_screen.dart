
import 'package:competences/utils/input_widget_auth.dart';
import 'package:competences/utils/mixed_widgets.dart';
import 'package:competences/utils/palettes.dart';
import 'package:flutter/material.dart';

class AuthScreen extends StatefulWidget {
  @override
  _State createState() => _State();
}
class _State extends State<AuthScreen> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController userEmail = TextEditingController();
  TextEditingController userPassword = TextEditingController();

  bool switchControl = false;
  var textHolder = 'Switch is OFF';

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        backgroundColor: Palette.secondary,
        body: SingleChildScrollView(
          child: new Container(
            width: double.infinity,
            height: screenSize.height,
            decoration: BoxDecoration(
              color: Palette.secondary,
              image: DecorationImage(
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(1), BlendMode.dstATop),
                image: AssetImage('images/commons/bg.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 70.0,
                ),
                Container(
                  padding: EdgeInsets.all(MediaQuery
                      .of(context)
                      .size
                      .width / 20),
                  child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          InputWidgetAuth(txController: userEmail, hint: 'Identifiant', prefixIcon: Icons.account_circle, isEmptyText: 'Veuillez entrer votre identifiant.', type: TextInputType.text, line: 1),
                          SizedBox(height: 10,),
                          InputWidgetAuth(txController: userPassword, hint: 'Mot de passe', prefixIcon: Icons.lock, isEmptyText: 'Veuillez entrer votre mot de passe.', type: TextInputType.text, line: 1, isPassField: true,),
                          SizedBox(height: 5,),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                  padding: const EdgeInsets.only(right: 20.0),
                                  child: FlatButton.icon(
                                      onPressed: () => {
                                        Navigator.pushReplacementNamed(context, "/")
                                      },
                                      icon: Icon(Icons.arrow_back),
                                      label: Text('Retour')
                                  )
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 15.0),
                                child: new FlatButton(
                                  child: new Text(
                                    "Mot de passe oublié ?",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Palette.secondary,
                                      fontSize: 12.0,
                                    ),
                                    textAlign: TextAlign.end,
                                  ),
                                  onPressed: () => {},
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10,),
                          FlatButton(
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                            ),
                            color: Palette.secondary,
                            onPressed: _validateForm,
                            child: new Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 10.0,
                                horizontal: 10.0,
                              ),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Expanded(
                                    child: Text(
                                      "Valider",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Palette.primary,
                                          fontWeight: FontWeight.bold, fontSize: 18),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      )
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }

  _validateForm() async {
    if (_formKey.currentState.validate()) {
      Navigator.pushReplacementNamed(context, "/home");
    }
  }
}
