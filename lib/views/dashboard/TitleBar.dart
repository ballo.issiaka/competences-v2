import 'package:competences/utils/T2Constant.dart';
import 'package:competences/utils/T2Extension.dart';
import 'package:competences/utils/T2Widgets.dart';
import 'package:competences/utils/palettes.dart';
import 'package:flutter/material.dart';

class TitleBar extends StatefulWidget {
  var titleName;

  TitleBar(var this.titleName);

  @override
  State<StatefulWidget> createState() {
    return TitleBarState();
  }
}

class TitleBarState extends State<TitleBar> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        color: Palette.primary,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
              child: Center(
                child: Text(
            widget.titleName,
            maxLines: 2,
            style: TextStyle(
                  fontSize: 22, color: Palette.secondary),
                ),
              ),
            ),
            
          ],
        ),
      ),
    );
  }

  @override
  State<StatefulWidget> createState() {
    return null;
  }
}