
import 'package:competences/utils/T2DataGenerator.dart';
import 'package:competences/utils/T2Extension.dart';
import 'package:competences/utils/T2Strings.dart';
import 'package:competences/utils/T2Widgets.dart';
import 'package:competences/utils/T2Colors.dart';
import 'package:competences/utils/model.dart';
import 'package:competences/utils/palettes.dart';
import 'package:competences/views/dashboard/ServiceGrid.dart';
import 'package:competences/views/dashboard/TitleBar.dart';
import 'package:competences/views/dashboard/slideBuilder.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  static var tag = "/T2ImageSlider";


  @override
  DashboardState createState() => DashboardState();
}

class DashboardState extends State<Dashboard> {
  var currentIndexPage = 0;
  List<T2Slider> mSliderList;
  @override
  void initState() {
    super.initState();
    mSliderList = getSliders();

  }
  @override
  Widget build(BuildContext context) {
    changeStatusColor(t2White);

    var width = MediaQuery.of(context).size.width;
    width = width - 50;
    final Size cardSize = Size(width, width / 1.8);
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SliderWidget(),
            SizedBox(height: 16),
            Center(
              child: Image.asset('images/commons/logo.png', height:60, fit: BoxFit.cover)
            ),
            SizedBox(height: 16),
            TitleBar('Rubriques professionnelles'),
            ServiceGrid(),


          ],
        ),
      ),
    );
  }
}