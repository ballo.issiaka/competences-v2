
import 'package:competences/utils/model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:competences/utils/T2DataGenerator.dart';
import 'package:competences/utils/T2SliderWidget.dart';
class CardSliderWidget extends StatefulWidget {
  static String tag = '/T2Slider';
  @override
  CardSliderState createState() => CardSliderState();
}
class CardSliderState extends State<CardSliderWidget> {
  var currentIndexPage = 0;
  List<T2Slider> mSliderList;

  @override
  void initState() {
    super.initState();
    mSliderList = getSliders();

  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    width = width - 50;
    final Size cardSize = Size(width, width / 1.8);
    return Column(
      children: <Widget>[
        T2CarouselSlider(
          viewportFraction: 0.9,
          height: cardSize.height,
          enlargeCenterPage: true,
          scrollDirection: Axis.horizontal,
          items: mSliderList.map((slider) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                  width: MediaQuery.of(context).size.width,
                  height: cardSize.height,
                  margin: EdgeInsets.symmetric(horizontal: 8.0),
                  child: Stack(
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: new BorderRadius.circular(12.0),
                        child: Image.asset(
                          slider.image,
                          fit: BoxFit.fill,
                          width: MediaQuery.of(context).size.width,
                          height: cardSize.height,
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          }).toList(),
          onPageChanged: (index){
            setState(() {
              currentIndexPage=index;
            });
          },
        ),
        SizedBox(
          height: 16,
        ),
      ],
    );
  }
}