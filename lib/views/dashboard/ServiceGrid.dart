import 'package:competences/utils/palettes.dart';
import 'package:competences/views/listService.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class ServiceGrid extends StatefulWidget {
  @override
  _ServiceGridState createState() => _ServiceGridState();
}

class _ServiceGridState extends State<ServiceGrid> {
  List items;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    items = [ { 
                "name": "Santé & soins personnel",
                "items": ["Pharmacie", "Cliniques médicales", "Physiothérapeutes", "Spas"],
              },
              {
                "name": "Services pro",
                "items": ["Plombier","Électricien","Mécanicien","Frigoriste","Avocat" ,"Menuisier","Coiffeur","Esthéticien" ,"Quincaillerie","Rénovation" ,"Réparation d’électroménagers"],
              },
              {
                "name": "Resto",
                "items": ["Restaurant" ,"Boulangerie" ,"Maquis" ,"Pizzeria" ,"Salon de thé","Pâtisserie","Glaciers"  ,"Burgers","Cafétaria"],
                },
              {
                "name": "Hôtels & résidences",
                "items": ["Hôtels", "Hôtels Résidences", "Auberge"],
              },
              {
                "name": "Immobilier & BTP",
                "items": ["Agence immobilière",
"Cabinet de géomètre expert",
"Construction",
"Cabinet d’Architecte",
"Vitrerie" ,
"Menuiserie de bois",
"Menuiserie d’aluminium"
],
              },
              {
                "name": "Commerces/Bazar",
                "items": ["Salon de coiffure",
"Pressing" ,
"Supermarché",
"Boutique de vêtement",
"Boutique de chaussure",
"Vente d’automobile" ,
"Boucherie ",
"Literie",
"Bijouterie ",
"Galeries d'art"
],
              },
            ];
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.65,
      child: StaggeredGridView.countBuilder(
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 4,
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) => InkWell(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=> ServicesList(services: items[index],)));
          },
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: Palette.primary, width: 5.0),
              color: Palette.secondary,
            ),
            child: new Center(
              child: Text('${items[index]['name']}',style:TextStyle(color: Palette.primary,), textAlign: TextAlign.center,),
            )),
        ),
        staggeredTileBuilder: (int index) =>
            new StaggeredTile.count(2,1),
        mainAxisSpacing: 4.0,
        crossAxisSpacing: 4.0,
      ),
    );
  }
}