import 'package:competences/utils/T2Colors.dart';
import 'package:competences/utils/T2DataGenerator.dart';
import 'package:competences/utils/T2Extension.dart';
import 'package:competences/utils/T2Images.dart';
import 'package:competences/utils/bottom_navigation.dart';
import 'package:competences/utils/images_template.dart';
import 'package:competences/utils/model.dart';
import 'package:competences/utils/palettes.dart';
import 'package:competences/views/dashboard/dashboard.dart';
import 'package:competences/views/profile/profile_screen.dart';
import 'package:competences/views/user/signup_signin.dart';
import 'package:competences/views/notifications/notifications.dart';
import 'package:competences/views/searchArea/search.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatefulWidget {
  static const String routeName = '/home';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool passwordVisible = false;
  bool isRemember = false;
  var currentIndexPage = 0;
  List<T2Favourite> mFavouriteList;
  List<T2Slider> mSliderList;

  @override
  void initState() {
    super.initState();
    mFavouriteList = getFavourites();
    mSliderList = getSliders();
  }

  var currentIndex = 0;
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  void changePage(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  void changeSldier(int index) {
    setState(() {
      currentIndexPage = index;
    });
  }
  void requestRebuild() {
    if(mounted) setState(() {});
  }
  @override
  Widget build(BuildContext context) {
    changeStatusColor(t2TextColorPrimary);
    var width = MediaQuery.of(context).size.width;
    width = width - 50;
    final Size cardSize = Size(width, width / 1.5);
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();
    return Scaffold(
      key: _scaffoldKey,
     // drawer: Drawer(child: Center(child: Text('operational drawer'),),),
      body: OfflineBuilder(
        connectivityBuilder: (BuildContext context, ConnectivityResult connectivity,Widget child){
          final bool connected = connectivity != ConnectivityResult.none;
          Widget thechild = (connected)
              ?
          SingleChildScrollView(
            padding: EdgeInsets.only(top: 45),
            physics: ScrollPhysics(),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: PageView(
                physics:new NeverScrollableScrollPhysics(),
                controller: pageController,
                children: <Widget>[
                  Dashboard(),
                  ResearchScreen(),

                  //T2SignIn()

                  NotificationScreen(),
                  ProfileScreen(),

                ],
              ),
            ),
          )
          :
          Center(child: Text("aucune connexion internet trouvé"),);
          return thechild;
        },
        child: Center(child: Text("aucune connexion internet trouvé"),),
      ),
      bottomNavigationBar: CurvedNavigationBar(
        backgroundColor: Palette.secondary,
        color: Palette.primary,
        items: <Widget>[
          SvgPicture.asset(
            t3_ic_home,
            color: Palette.secondary,
            height: 24,
            width: 24,
            fit: BoxFit.none,
          ),
          SvgPicture.asset(
            t2_search,
            color: Palette.secondary,
            height: 24,
            width: 24,
            fit: BoxFit.none,
          ),
          SvgPicture.asset(
            t3_notification,
            color: Palette.secondary,
            height: 24,
            width: 24,
            fit: BoxFit.none,
          ),
          SvgPicture.asset(
            t3_ic_user,
            color: Palette.secondary,
            height: 24,
            width: 24,
            fit: BoxFit.none,
          ),
        ],
        onTap: (index) {
          //Handle button tap
          
          currentIndex = index;
          pageController.animateToPage(index, duration: Duration(milliseconds: 500), curve: Curves.ease);
          
        },
      ),
    );
  }
}
