import 'package:competences/utils/constants.dart';
import 'package:competences/utils/input_widget_auth.dart';
import 'package:competences/utils/palettes.dart';
import 'package:competences/utils/title_widget.dart';
import 'package:flutter/material.dart';


class RegClientFinalScreen extends StatefulWidget {
  var data;
  RegClientFinalScreen({this.data});

  @override
  _State createState() => _State();
}
class _State extends State<RegClientFinalScreen> {
  final _formKey0 = GlobalKey<FormState>();

  TextEditingController userContact = TextEditingController();
  TextEditingController userEmail = TextEditingController();
  TextEditingController userPassword = TextEditingController();
  TextEditingController userPassConf = TextEditingController();
  bool cguCheckbox = false;

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    String dropdownValue = 'Homme'; // Option 2

    // TODO: implement build
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        backgroundColor: Palette.secondary,
        body: SingleChildScrollView(
          child: new Container(
            width: double.infinity,
            decoration: BoxDecoration(
              color: Palette.secondary,
              image: DecorationImage(
                colorFilter: new ColorFilter.mode(
                    Palette.secondary.withOpacity(0.0), BlendMode.dstATop),
                image: AssetImage('images/commons/bg.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: new Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(screenSize.width/5),
                  child: Center(
                    child: TitleWidget(title: 'Etape finale',),
                  ),
                ),
                Container(
                  width: screenSize.width,
                  padding: EdgeInsets.only(left:screenSize.width/15, right: screenSize.width/15),
                  child: Form(
                      key: _formKey0,
                      child: Column(
                        children: <Widget>[
                          InputWidgetAuth(txController: userContact, hint: 'Contact', prefixIcon: Icons.contacts, isEmptyText: 'Veuillez entrer votre contact.', type: TextInputType.text, line: 1),
                          SizedBox(height: 20,),
                          InputWidgetAuth(txController: userEmail, hint: 'Email', prefixIcon: Icons.email, isEmptyText: 'Veuillez entrer votre adresse email.', type: TextInputType.text, line: 1,),
                          SizedBox(height: 15,),
                          InputWidgetAuth(txController: userPassword, hint: 'Mot de passe', prefixIcon: Icons.lock, isEmptyText: 'Veuillez entrer votre mot de passe.', type: TextInputType.text, line: 1, isPassField: true,),
                          SizedBox(height: 15,),
                          InputWidgetAuth(txController: userPassConf, hint: 'Confirmation de mot de passe', prefixIcon: Icons.lock, isEmptyText: 'Veuillez confirmer votre mot de passe.', type: TextInputType.text, line: 1, isPassField: true,),
                          SizedBox(height: 15,),
                          Container(
                            decoration: ShapeDecoration(
                              color: Colors.white70,
                              shape: RoundedRectangleBorder(
                                side: BorderSide(width: 1.0, style: BorderStyle.solid),
                                borderRadius: BorderRadius.all(Radius.circular(30.0)),
                              ),
                            ),
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: dropdownValue,
                              icon: Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(
                                color: Palette.secondary,
                                fontSize: 18,
                              ),
                              underline: Container(
                                color: Colors.transparent,
                              ),
                              onChanged: (String newValue) {
                                setState(() {
                                  dropdownValue = newValue;
                                });
                              },
                              items: <String>['Homme', 'Femme']
                                  .map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              })
                                  .toList(),
                            ),
                          ),
                          SizedBox(height: 15,),
                          Row(
                            children: <Widget>[
                              Checkbox(
                                value: cguCheckbox,
                                onChanged: (bool value) {
                                  setState(() {
                                    cguCheckbox = value;
                                  });
                                },
                              ),
                              Flexible(
                                child: Text("J'ai lu et j'accepte les conditions générales d'utilisation (CGU)", style: TextStyle(color: Colors.white),),
                              ),
                            ],
                          ),
                          SizedBox(height: 15,),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 15.0),
                                child: new FlatButton(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Icon(Icons.arrow_back, color: Palette.white,),
                                      Text(
                                        "Précédent",
                                        style: TextStyle(
                                          color: Palette.white,
                                          fontSize: 16.0,
                                        ),
                                        textAlign: TextAlign.end,
                                      )
                                    ],
                                  ),
                                  onPressed: () => {
                                    Navigator.pop(context)
                                  },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 15.0),
                                child: new FlatButton(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Valider",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Palette.primary,
                                          fontSize: 18.0,
                                        ),
                                        textAlign: TextAlign.end,
                                      ),
                                      Icon(Icons.arrow_forward, color: Palette.primary,)
                                    ],
                                  ),
                                  onPressed: (){
                                    _validateForm();
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                  ),
                ),
                SizedBox(
                  height: 24.0,
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }

  _validateForm() async {
    if (_formKey0.currentState.validate()) {
      if (cguCheckbox) {
        Navigator.pushReplacementNamed(context, "/home");
      } else {
        print('User must accept contract');
      }
    } else {
      print('error');
    }
  }
}
