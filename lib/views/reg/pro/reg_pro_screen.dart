import 'package:competences/utils/constants.dart';
import 'package:competences/utils/input_widget_auth.dart';
import 'package:competences/utils/palettes.dart';
import 'package:competences/utils/title_widget.dart';
import 'package:flutter/material.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/cupertino.dart';

import 'reg_pro_final_screen.dart';


class RegProScreen extends StatefulWidget {
  @override
  _State createState() => _State();
}
class _State extends State<RegProScreen> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController userLabel = TextEditingController();
  TextEditingController userFunction = TextEditingController();
  TextEditingController userSpeciality = TextEditingController();
  TextEditingController userCountry = TextEditingController();
  TextEditingController userCity = TextEditingController();
  TextEditingController userCommune = TextEditingController();
  TextEditingController userQuartier = TextEditingController();

  Country _selectedCupertinoCountry = CountryPickerUtils.getCountryByIsoCode('ci');

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    // TODO: implement build
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        backgroundColor: Palette.secondary,
        body: SingleChildScrollView(
          child: new Container(
            width: double.infinity,
            decoration: BoxDecoration(
              color: Palette.secondary,
              image: DecorationImage(
                colorFilter: new ColorFilter.mode(
                    Palette.secondary.withOpacity(0.0), BlendMode.dstATop),
                image: AssetImage('images/commons/bg.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: new Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(screenSize.width/10),
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        TitleWidget(title: 'Création',),
                        TitleWidget(title: 'Compte Professionnel', size: 18,)
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left:screenSize.width/15, right: screenSize.width/15),
                  child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          InputWidgetAuth(txController: userLabel, hint: 'Nom & Prénoms / Entreprise', prefixIcon: Icons.account_circle, isEmptyText: 'Ce champ est requis.', type: TextInputType.text, line: 1),
                          SizedBox(height: 20,),
                          InputWidgetAuth(txController: userFunction, hint: 'Domaine d\'activité / Profession', prefixIcon: Icons.account_circle, isEmptyText: 'Ce champ est requis.', type: TextInputType.text, line: 1, ),
                          SizedBox(height: 15,),
                          InputWidgetAuth(txController: userSpeciality, hint: 'Spécialité', prefixIcon: Icons.account_circle, isEmptyText: 'Ce champ est requis.', type: TextInputType.text, line: 1, ),
                          SizedBox(height: 15,),
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            color: Colors.white70,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ListTile(
                                  leading: Icon(Icons.account_balance, color: Palette.secondary,),
                                  title: _buildCupertinoSelectedItem(_selectedCupertinoCountry),
                                  onTap: _openCupertinoCountryPicker,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 15,),
                          InputWidgetAuth(txController: userCity, hint: 'Ville', prefixIcon: Icons.location_city, isEmptyText: 'Ce champ est requis.', type: TextInputType.text, line: 1,),
                          SizedBox(height: 15,),
                          InputWidgetAuth(txController: userCommune, hint: 'Commune', prefixIcon: Icons.add_location, isEmptyText: 'Ce champ est requis.', type: TextInputType.text, line: 1, ),
                          SizedBox(height: 15,),
                          InputWidgetAuth(txController: userQuartier, hint: 'Quartier', prefixIcon: Icons.add_location, isEmptyText: 'Ce champ est requis.', type: TextInputType.text, line: 1),
                          SizedBox(height: 15,),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 15.0),
                                child: new FlatButton(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Icon(Icons.arrow_back, color: Colors.grey,),
                                      Text(
                                        "Annuler",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey,
                                          fontSize: 16.0,
                                        ),
                                        textAlign: TextAlign.end,
                                      )
                                    ],
                                  ),
                                  onPressed: () => {
                                    Navigator.pop(context)
                                  },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 15.0),
                                child: new FlatButton(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Suivant",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Palette.primary,
                                          fontSize: 16.0,
                                        ),
                                        textAlign: TextAlign.end,
                                      ),
                                      Icon(Icons.arrow_forward, color: Palette.primary,)
                                    ],
                                  ),
                                  onPressed: () => {
                                    if(_formKey.currentState.validate()) {
                                      Navigator.push(context, MaterialPageRoute(
                                        builder: (context) => RegProFinalScreen(data: []),
                                      ))
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                  ),
                ),
                SizedBox(
                  height: 24.0,
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildCupertinoSelectedItem(Country country) {
    return Row(
      children: <Widget>[
        Text("+${country.phoneCode}"),
        SizedBox(width: 8.0),
        Flexible(child: Text(country.name))
      ],
    );
  }
  void _openCupertinoCountryPicker() => showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return CountryPickerCupertino(
          backgroundColor: Colors.black,
          itemBuilder: _buildCupertinoItem,
          pickerSheetHeight: 300.0,
          pickerItemHeight: 75,
          initialCountry: _selectedCupertinoCountry,
          onValuePicked: (Country country) => {
            setState(() => _selectedCupertinoCountry = country),
          },
        );
      });
  Widget _buildCupertinoItem(Country country) {
    return DefaultTextStyle(
      style:
      const TextStyle(
        color: CupertinoColors.white,
        fontSize: 16.0,
      ),
      child: Row(
        children: <Widget>[
          SizedBox(width: 8.0),
          Text("+${country.phoneCode}"),
          SizedBox(width: 8.0),
          Flexible(child: Text(country.name))
        ],
      ),
    );
  }
}
