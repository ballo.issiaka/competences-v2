import 'package:competences/utils/mixed_widgets.dart';
import 'package:competences/utils/palettes.dart';
import 'package:competences/utils/title_widget.dart';
import 'package:flutter/material.dart';

class RegTypeScreen extends StatefulWidget {
  @override
  _State createState() => _State();
}
class _State extends State<RegTypeScreen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Palette.secondary,
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: Palette.secondary,
          image: DecorationImage(
            colorFilter: new ColorFilter.mode(
                Palette.secondary.withOpacity(1.0), BlendMode.dstATop),
            image: AssetImage('images/commons/bg.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 50,),
            TitleWidget(title: 'Type de compte'),
            clientButton(),
            proButton()
          ],
        ),
      ),
    );
  }
  Widget clientButton () {
    Size screenSize = MediaQuery.of(context).size;
    return new Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(left: screenSize.width/15, right: screenSize.width/15, top: screenSize.width/15),
      alignment: Alignment.center,
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new FlatButton(
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
              color: Palette.secondary,
              onPressed: () => {
                Navigator.pushNamed(context, '/reg/client')
              },
              child: new Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 20.0,
                  horizontal: 20.0,
                ),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Expanded(
                      child: Text(
                        "Client",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Palette.primary,
                            fontWeight: FontWeight.bold,
                            fontSize: 21
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
  Widget proButton () {
    Size screenSize = MediaQuery.of(context).size;
    return new Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(left: screenSize.width/15, right: screenSize.width/15, top: screenSize.width/15),
      alignment: Alignment.center,
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new FlatButton(
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
              color: Palette.secondary,
              onPressed: () => {
                Navigator.pushNamed(context, '/reg/pro')
              },
              child: new Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 20.0,
                  horizontal: 20.0,
                ),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Expanded(
                      child: Text(
                        "Professionel",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Palette.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 21
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
