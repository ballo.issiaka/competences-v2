import 'package:flutter/material.dart';
import 'package:competences/views/notifications/block_sample.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _State createState() => _State();
}
class _State extends State<NotificationScreen> {
  final _formKey = GlobalKey<FormState>();

  var searchBox = TextEditingController();


  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    // TODO: implement build
    return SingleChildScrollView(
      child: Container(
        width: screenSize.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(screenSize.width/20),
              child: Text('Notifications', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                primary: false,
                padding: EdgeInsets.all(4),
                itemCount: 15,
                itemBuilder: (BuildContext context, int index) {
                  var stat = 1;

                  if(index == 3 || index == 7 || index == 8) {
                    stat = 0;
                  }

                  return BlockSample(stat: stat, type: 'notifications', func: () => {

                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}