import 'package:competences/utils/palettes.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class BlockSample extends StatefulWidget {
  var type, func, stat;
  BlockSample({this.type, this.func, this.stat});

  @override
  _State createState() => _State();
}
class _State extends State<BlockSample> {
  @override
  Widget build(BuildContext context) {
    if (widget.type == 'services') {
      return _buildService();
    } else if (widget.type == 'notifications') {
      return _tileNotification();
    } else if (widget.type == 'worker') {
      return _tileWorker();
    } else if (widget.type == 'drugstore') {
      return _buildDrugstore();
    } else {
      return _buildDrugstore();
    }
  }
  Widget _tileNotification(){
    var statColor = Palette.dark;

    if(widget.stat == 0) {
      statColor = Palette.primary;
    }
    return Card(
      child: InkWell(
        onTap: () {
          widget.func();
        },
        child: ListTile(
          title: Text('Titre notification'),
          subtitle: Text('Description de la notification'),
          leading: Icon(Icons.notifications, size: 50, color: statColor,),
          trailing: Text('il y 10 min'),
        ),
      ),
    );
  }
  Widget _tileWorker(){
    return Card(
      child: InkWell(
        onTap: () {
          widget.func();
        },
        child: ListTile(
          title: Text('Libéllé service'),
          subtitle: Text('service délivré par John Doe'),
          leading: Icon(Icons.supervisor_account, size: 50,),
          trailing: Text('il y 10 min'),
        ),
      ),
    );
  }
  Widget _buildService(){
    return Card(
      child: InkWell(
        onTap: () {
          widget.func();
        },
        child: ListTile(
          title: Text('Libéllé du service'),
          subtitle: Text('Nom du prestataire'),
          leading: Icon(Icons.local_library, size: 50,),
          trailing: Text('il y 10 min'),
        ),
      ),
    );
  }
  Widget _buildDrugstore(){
    return Card(
      child: InkWell(
        onTap: () {
          widget.func();
        },
        child: ListTile(
          title: Text('Pharmacie de l\'Azerty', style: TextStyle(fontSize: 18),),
          subtitle: Text('22e Arrondissement, Angré Cocody', style: TextStyle(fontSize: 18),),
          leading: Icon(Icons.store, size: 50,),
        ),
      ),
    );
  }
}