import 'package:competences/utils/service_slide.dart';
import 'package:competences/views/dashboard/TitleBar.dart';
import 'package:competences/views/dashboard/slideBuilder.dart';
import 'package:competences/utils/palettes.dart';
import 'package:flutter/material.dart';

class ServicesList extends StatefulWidget {
  var services;
  @override
  _ServicesListState createState() => _ServicesListState();
  ServicesList({this.services});
}




class _ServicesListState extends State<ServicesList> {


  Widget buildItems(){
    print(widget.services['items']);
  return Column(children: widget.services['items'].map<Widget>((item){
              return Column(children: <Widget>[
                  Container(
              padding: EdgeInsets.all(8.0),
              constraints: BoxConstraints(minWidth: double.infinity),
              
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                Text('$item', style: TextStyle(color: Palette.dark, fontSize: 14.0),),
                FlatButton(
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0),
                  ),
                  color: Palette.secondary,
                  onPressed: (){},
                  child: new Container(
                    child: Text(
                      "voir plus >",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Palette.primary,
                        fontWeight: FontWeight.bold, fontSize: 12),
                    ),
                  ),
                )
              ],),
            ),
            SizedBox(height: 15),
            ServiceSlide(),
            SizedBox(height: 15),
              ],);
            }).toList()
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('${widget.services['name']}'),),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SliderWidget(),
            SizedBox(height: 16),
            Center(
              child: Image.asset('images/commons/logo.png', height:60, fit: BoxFit.cover)
            ),
            SizedBox(height: 16),
            buildItems(),
            SizedBox(height: 15),


          ],
        ),
      )
    );
  }
}