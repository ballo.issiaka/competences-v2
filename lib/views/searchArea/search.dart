import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ResearchScreen extends StatefulWidget {
  @override
  _State createState() => _State();
}
class _State extends State<ResearchScreen> {
  final _formKey = GlobalKey<FormState>();

  var searchBox = TextEditingController();

  // Google Map
  Completer<GoogleMapController> _controller = Completer();
  static const LatLng _center = const LatLng(5.3563135, -4.0334255);

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }
  // Google Map END

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Container(
        width: screenSize.width,
        child: Stack(
          children: <Widget>[
            // Replace this container with your Map widget
            GoogleMap(
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: _center,
                zoom: 13.0,
              ),
            ),
            Positioned(
              top: 10,
              right: 15,
              left: 15,
              child: Container(
                color: Colors.white,
                child: Row(
                  children: <Widget>[
                    IconButton(
                      splashColor: Colors.grey,
                      icon: Icon(Icons.search),
                      onPressed: () {},
                    ),
                    Expanded(
                      child: TextField(
                        cursorColor: Colors.black,
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.go,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding:
                            EdgeInsets.symmetric(horizontal: 15),
                            hintText: "Rechercher..."),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
